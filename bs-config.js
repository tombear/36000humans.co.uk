module.exports = {
  server: true,
  serveStatic: ["./public"],
  serveStaticOptions: {
    extensions: ["html"]
  },
  files: ["./public/**"],
  watch: true
};
