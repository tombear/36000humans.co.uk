#!/usr/bin/env node
const fs = require("fs");
const fsx = require("fs-extra");
const cheerio = require("cheerio");
const sass = require("node-sass");

const mapPipe = (...fs) => xs => fs.reduce((acc, f) => acc.map(f), xs);
const readFile = x => fs.readFileSync(x, "utf8");
const writeFile = path => data => fs.writeFileSync(path, data, "utf8");
const readdir = x => fs.readdirSync(x);
const mkdir = x => fs.existsSync(x) ? x : (fs.mkdirSync(x), x);

// const cfg = {
//   shellFile: "./src/html/templates/shell.html",
//   pagesDir: "./src/html/pages/",
//   assetsDir: "./src/assets/",
//   scssDir: "./src/scss/"
// }
if (process.env.DEV) {
  console.log("hi!")
}

const shellHTML = readFile("./src/html/templates/shell.html");
const $ = cheerio.load(shellHTML);
const wrap = x => {
  $("main#root").empty();
  $("main#root").append(x);
  return $.html();
}

function closeMe(paths) {
  mkdir("./public");
  const outputHTMLs =
    mapPipe(
      x => `./src/html/pages/${x}`,
      readFile,
      wrap
    )(paths);

  for (let i = 0; i < paths.length; i++) {
    writeFile(`./public/${paths[i]}`)(outputHTMLs[i]);
  }
  sass.render({
    file: "./src/scss/styles.scss",
    outFile: "./public/styles.css"
  }, function(err, res) {
    fs.writeFile("./public/styles.css", res.css, function (err) {
      if (err) console.log(`zomg err: ${err}`);
    });
  });
  fsx.copySync("./src/assets/", "./public/assets/", { overwrite: true })
}

closeMe(readdir("./src/html/pages"));
